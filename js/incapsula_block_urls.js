var app = angular.module("incapsulaApp", []);
app.controller("ctrlblockurls", function ($scope, $http) {
    $scope.rows = [];
    $scope.temp = false;
    $scope.saving = false;
    $scope.addRegion == "";

    $http.get('/incapsula/endpoint/config')
        .then(function (res) {
            var acls = res.data.security.acls.rules;
            for (var i = 0; i < acls.length; i++) {
                if (acls[i].id == 'api.acl.blacklisted_urls') {
                    var urls = acls[i].urls;
                    for (var i = 0; i < urls.length; i++) {
                        $scope.rows.push({
                            url: urls[i].value,
                            pattern: urls[i].pattern
                        });
                    }
                    break;
                }
            }
            $scope.dataLoaded = true;
        });

    $scope.addRow = function () {
        $scope.temp = false;
        $scope.addUrl = "";
        $scope.addPattern = "EQUALS";
    };

    $scope.deleteRow = function (row) {
        $scope.rows.splice($scope.rows.indexOf(row), 1);
    };

    $scope.plural = function (tab) {
        return (tab.length > 1 || tab.length == 0) ? 's' : '';
    };

    $scope.addTemp = function () {
        if ($scope.temp) $scope.rows.pop();
        else if ($scope.addUrl) $scope.temp = true;
        if ($scope.addUrl) {
            $scope.rows.push({
                url: $scope.addUrl,
                pattern: $scope.addPattern
            });
        }
        else {
            $scope.temp = false;
        }
    };

    $scope.isTemp = function (index) {
        return $scope.rows[index].url == $scope.addUrl;
    };

    $scope.saveForm = function (rows) {
        if ($scope.temp == false && $scope.addUrl == "") {
            $scope.saving = true;

            var url = [];
            var pattern = [];
            jQuery.each( rows, function( key, value ) {
                url.push(encodeURIComponent(value.url));
                pattern.push(value.pattern);
            });

             $http({
             method: 'POST',
             url: '/incapsula/endpoint/blockurls',
             data: "urls=" + url.join(",") + "&pattern=" + pattern.join(","),
             headers: {'Content-Type': 'application/x-www-form-urlencoded'}
             }).then(function (res) {
             $scope.saving = false;
             });
        }
    };
});
