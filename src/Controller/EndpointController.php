<?php

namespace Drupal\incapsula_purge\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Drupal\incapsula_purge\Incapsula;

/**
 * Class blockCountries.
 */
class EndpointController {
	public function content($endpoint) {
		switch ($endpoint) {
			case 'config':
				$this->_incapsula_endpoint_drupal_config();
				break;
			case 'whitelist':
				$this->_incapsula_endpoint_drupal_whitelist();
				break;
			case 'blockips':
				$this->_incapsula_endpoint_drupal_blockips();
				break;
			case 'blockurls':
				$this->_incapsula_endpoint_drupal_blockurls();
				break;
			case 'blockcountry':
				$this->_incapsula_endpoint_drupal_blockcountry();
				break;
			default:
				return new JsonResponse('Invalid endpoint!!');
		}
		// return new JsonResponse($endpoint);
		// return new JsonResponse([ 'data' => $this->getData(), 'method' => 'GET', 'status'=> 200]);
	}

	/**
	 * Helper function to fetch the configuration from Incapsula.
	 */
	public function _incapsula_endpoint_drupal_config() {
		if ($inc = $this->_incapsula_object_construct()) {
			dump($inc->config());
			// echo drupal_json_output($inc->config());
		}
		else {
			return new JsonResponse(['error' => 'Incapsula isn\'t configured, go the configuration page to resolve this'], 200);
			// echo drupal_json_output(['error' => 'Incapsula isn\'t configured, go the configuration page to resolve this']);
		}
	}

	/**
	 * Helper function to manage the IP whitelist.
	 */
	public function _incapsula_endpoint_drupal_whitelist() {
		$ips = isset($_POST['ips']) ? $_POST['ips'] : "";
		if ($inc = $this->_incapsula_object_construct()) {
			dump($inc->acl(INCAPSULA_ACL_WHITELIST_IP, $ips));
			echo drupal_json_output($inc->acl(INCAPSULA_ACL_WHITELIST_IP, $ips));
		}
		else {
			return new JsonResponse(['error' => 'Incapsula isn\'t configured, go the configuration page to resolve this'], 200);
		}
	}

	/**
	 * Helper function to manage the IP blacklist.
	 */
	public function _incapsula_endpoint_drupal_blockips() {
		$ips = isset($_POST['ips']) ? $_POST['ips'] : "";
		if ($inc = $this->_incapsula_object_construct()) {
			dump($inc->acl($inc->acl(INCAPSULA_ACL_BLACKLIST_IP, $ips)));
			echo drupal_json_output($inc->acl(INCAPSULA_ACL_BLACKLIST_IP, $ips));
		}
		else {
			return new JsonResponse(['error' => 'Incapsula isn\'t configured, go the configuration page to resolve this'], 200);
		}
	}

	/**
	 * Helper function to manage the blocked URLs.
	 */
	public function _incapsula_endpoint_drupal_blockurls() {
		$urls = isset($_POST['urls']) ? $_POST['urls'] : "";
		$pattern = isset($_POST['pattern']) ? $_POST['pattern'] : "";
		if ($inc = $this->_incapsula_object_construct()) {
			dump($inc->acl($inc->acl(INCAPSULA_ACL_BLACKLIST_URLS, [
				'urls' => str_replace("%2C", ",", urlencode($urls)),
				'url_patterns' => $pattern
			])));
			echo drupal_json_output($inc->acl(INCAPSULA_ACL_BLACKLIST_URLS, [
				'urls' => str_replace("%2C", ",", urlencode($urls)),
				'url_patterns' => $pattern
			]));
		}
		else {
			return new JsonResponse(['error' => 'Incapsula isn\'t configured, go the configuration page to resolve this'], 200);
		}
	}

	/**
	 * Helper function to manage the blocked countries/contintents.
	 */
	public function _incapsula_endpoint_drupal_blockcountry() {
		$countries = isset($_POST['countries']) ? $_POST['countries'] : "";
		$continents = isset($_POST['continents']) ? $_POST['continents'] : "";
		if ($inc = $this->_incapsula_object_construct()) {
			$json_data = $inc->acl(INCAPSULA_ACL_BLACKLIST_COUNTRY, [
				'countries' => $countries,
				'continents' => $continents
			]);
			// dump(new JsonResponse($json_data));
			// return new JsonResponse($json_data);
			// echo drupal_json_output($inc->acl(INCAPSULA_ACL_BLACKLIST_COUNTRY, [
			// 	'countries' => $countries,
			// 	'continents' => $continents
			// ]));
		}
		else {
			return new JsonResponse(['error' => 'Incapsula isn\'t configured, go the configuration page to resolve this'], 200);
			// echo drupal_json_output(['error' => 'Incapsula isn\'t configured, go the configuration page to resolve this']);
		}
	}

	public function _incapsula_object_construct() {
		$config = \Drupal::config('incapsula_purge.incapsulasettings');
		$api_id = $config->get('api_id');
		$api_key = $config->get('api_key');
		$site_id = $config->get('site_id');
		$api_end_point = $config->get('api_end_point');
		
		if (!is_null($api_id) && !is_null($api_key) && !is_null($site_id) && !is_null($api_end_point)) {
			return new Incapsula($api_id, $api_key, $site_id, $api_end_point);
		}
		else {
			\Drupal::messenger()->addMessage(t("Incapsula isn\'t configured, go the configuration page to resolve this"), 'status', FALSE);
			return FALSE;
		}
	}
}
