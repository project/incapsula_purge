<?php

namespace Drupal\incapsula_purge\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class whiteListSettings.
 */
class whiteListSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'incapsula_purge.whitelistsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'white_list_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('incapsula_purge.whitelistsettings');
    $form['hello'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hello'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('hello'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('incapsula_purge.whitelistsettings')
      ->set('hello', $form_state->getValue('hello'))
      ->save();
  }

}
