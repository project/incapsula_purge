<?php

namespace Drupal\incapsula_purge\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class incapsulaSettingsForm.
 */
class incapsulaSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'incapsula_purge.incapsulasettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'incapsula_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('incapsula_purge.incapsulasettings');
	$form['incapsula'] = [
		'#type' => 'details',
		'#title' => $this->t('Incapsula General configuration'),
    '#open' => TRUE,
	];
    $form['incapsula']['api_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API ID'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('api_id'),
    ];
    $form['incapsula']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#maxlength' => 265,
      '#size' => 64,
      '#default_value' => $config->get('api_key'),
    ];
    $form['incapsula']['site_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site ID'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('site_id'),
    ];
    $form['incapsula']['api_end_point'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API End Point'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('api_end_point') ? $config->get('api_end_point') : 'https://my.incapsula.com/api/prov/v1',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('incapsula_purge.incapsulasettings')
      ->set('api_id', $form_state->getValue('api_id'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('site_id', $form_state->getValue('site_id'))
      ->set('api_end_point', $form_state->getValue('api_end_point'))
      ->save();
  }

}
