<?php

namespace Drupal\incapsula_purge\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class blockCountries.
 */
class blockCountries extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'incapsula_purge.blockcountries',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'block_countries';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('incapsula_purge.blockcountries');
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('incapsula_purge.blockcountries')
      ->save();
  }

}
