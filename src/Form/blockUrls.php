<?php

namespace Drupal\incapsula_purge\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class blockUrls.
 */
class blockUrls extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'incapsula_purge.blockurls',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'block_urls';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('incapsula_purge.blockurls');
    $form['hello1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hello1'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('hello1'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('incapsula_purge.blockurls')
      ->set('hello1', $form_state->getValue('hello1'))
      ->save();
  }

}
