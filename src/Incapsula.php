<?php

namespace Drupal\incapsula_purge;
define('INCAPSULA_ACL_BLACKLIST_COUNTRY', 'api.acl.blacklisted_countries');
define('INCAPSULA_ACL_BLACKLIST_URLS', 'api.acl.blacklisted_urls');
define('INCAPSULA_ACL_BLACKLIST_IP', 'api.acl.blacklisted_ips');
define('INCAPSULA_ACL_WHITELIST_IP', 'api.acl.whitelisted_ips');

class Incapsula {

    /**
   * The API Endpoint.
   */
  protected $api_endpoint;
    /**
   * The API ID.
   */
  protected $api_id;
    /**
   * The API Key.
   */
  protected $api_key;
    /**
   * The API Site ID.
   */
  protected $site_id;

  /**
   *
   */
  function __construct($api_id = NULL, $api_key = NULL, $site_id = NULL, $api_endpoint = NULL) {
    $this->api_id = $api_id;
    $this->api_key = $api_key;
    $this->site_id = $site_id;
    $this->api_endpoint = $api_endpoint;
  }

  /**
   * Purge callback to clear cache for a page or resource.
   *
   * @param string $pattern
   *   The pattern or exact string to purge from incapsula cache.
   *
   * @param string $site_id
   *   The site id as defined in incapsula allowing an override.
   *
   * @return mixed
   *   Returns a response-array from incapsula.
   */
  public function purge($pattern = NULL, $site_id = NULL) {
    $params = $this->settings($site_id);
    $params['purge_pattern'] = !is_null($pattern) ? $pattern : '';

    return $this->api_call('/sites/cache/purge', $params);
  }

  public function config($site_id = NULL) {
    $params = $this->settings($site_id);
    return $this->api_call('/sites/status', $params);

  }

  public function acl($rule = NULL, $options = "", $deviate = FALSE, $fetch = TRUE, $site_id = NULL) {
    $default_acl = [
      'rule_id' => $rule,
      'continents' => '',
      'countries' => '',
      'url_patterns' => '',
      'urls' => '',
      'ips' => '',
    ];
    $params = array_merge($this->settings($site_id), $default_acl);
    switch ($rule) {
      case INCAPSULA_ACL_BLACKLIST_COUNTRY:
        if (!(is_array($options))) {
          watchdog('Incapsula', 'API call for @rule requires an array.', ['@rule' => $rule], WATCHDOG_ERROR);
          return;
        }
        if (isset($options['countries'])) {
          $params['countries'] = $options['countries'];
        }
        if (isset($options['continents'])) {
          $params['continents'] = $options['continents'];
        }
        break;

      case INCAPSULA_ACL_BLACKLIST_URLS:
        if (!(is_array($options))) {
          watchdog('Incapsula', 'API call for @rule requires an array.', ['@rule' => $rule], WATCHDOG_ERROR);
          return;
        }
        if (!isset($options['urls']) || !isset($options['url_patterns'])) {
          watchdog('Incapsula', 'API call for @rule requires an array with keys \'urls\' and \'url_patterns\'.', ['@rule' => $rule], WATCHDOG_ERROR);
          return;
        }
        $params['urls'] = $options['urls'];
        $params['url_patterns'] = $options['url_patterns'];
        break;

      case INCAPSULA_ACL_BLACKLIST_IP:
      case INCAPSULA_ACL_WHITELIST_IP:
        $params['ips'] = $options;
        break;
    }

    return $this->api_call('/sites/configure/acl', $params);
  }

  private function api_call($api_url, $params = [], $method = 'POST') {
    $endpoint = $this->api_endpoint . $api_url;
    $response = NULL;
    switch ($method) {
      case 'POST':
        $response = \Drupal::httpClient()->post($endpoint, [
          'form_params' => ['api_id'=> '46252',
                            'api_key' => 'c2b9f82e-2256-48c2-bfd0-a9a2fc701675',
                            'site_id'=> '49764877',
                          ],
            'headers' => [
              'Content-type' => 'application/x-www-form-urlencoded',
              'X-Requested-With' => 'XMLHttpRequest'
            ],
          ])->getBody()->getContents();
        break;
    }
    dump($response.res_message);
    if ($response->code == 200) {
      // Valid response, get pages and objects.
      $data = json_decode($response);
      return $data;
    }
    else {
      // watchdog('Incapsula', 'API call failed with status code @code', ['@code' => $response->code], WATCHDOG_ERROR);
      // Logs a notice
// \Drupal::logger('my_module')->notice($message);
// Logs an error
      \Drupal::logger('incapsula_purge')->error('API call failed with status code @code', ['@code' => $response]);
    }

  }

  private function settings($site_id = NULL) {
    return [
      'api_id' => $this->api_id,
      'api_key' => $this->api_key,
      'site_id' => is_null($site_id) ? $this->site_id : $site_id,
    ];
  }
}
